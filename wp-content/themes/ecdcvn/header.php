<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<div class="site-inner">
		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentysixteen' ); ?></a>

		<div class="rx-header-group">
			<div class="rx-header">
		     <div class="rx-ecdc-logo">
		        <a href="<?=get_site_url()?>" >
		          <div class="rx-ecdc-mainlogo" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/netacom2.jpg) no-repeat center center;"></div>
		        </a>
		      </div>
		      <div class="rx-header-top">
		        <div class="rx-header-logo">          
		        </div>
		        <div class="rx-header-navleft">
		          <div><span class="rx-icon-topnav rx-icon-phone"></span>028 3535 4016</div>
		          <div><span class="rx-icon-topnav rx-icon-email"></span>contact@netacom.vn</div>
		        </div>
		        <div class="rx-header-navright">
		          <div>
		            <a class='linktext' href="<?=get_site_url()?>">Trang chủ</a>
		          </div>
		          <div class='news'>
		            <a class='linktext' href="<?=get_site_url()?>/category/tin-tuc/">Tin tức</a>
		          </div>
		          <div class='tuyen'>
		            <a class='linktext' href="<?=get_site_url()?>/category/tuyen-dung/">Tuyển dụng</a>
		          </div>
		          <div class='lang'>
		            <a href="<?=get_site_url()?>" >
		              <div class="langimg" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/rx-ecdc-langviet.png) no-repeat center center;"></div>
		            </a>
		            <div class='langhover'>
		              <div class='langimghover'>
		                <a href="<?=get_site_url()?>" >
		                  <div class="langimghv" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/rx-ecdc-langviet.png) no-repeat center center;"></div>
		                </a>
		              </div>
		              <div class='langimghover'>
		                <a href="<?=get_site_url()?>" >
		                  <div class="langimghv" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/english.png) no-repeat center center;"></div>
		                </a>
		              </div>
		            </div>
		          </div>
		        </div>
		      </div>
		       
		    <div class="rx-header-bot">
		        <ul class="rx-header-navmain">
		          	<li class='header-menu-content'>
		          		<a class='linktexthd' href="<?=get_site_url()?>/ve-chung-toi/">VỀ CHÚNG TÔI</a>
						<ul class='ushover'>
		              	<li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/ve-chung-toi/">TỔNG QUAN</a></li>
		              	<li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/nhung-chan-duong-netacom/">NHỮNG CHẶN ĐƯỜNG NETACOM</a></li>
		           		<li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/so-do-to-chuc/">SƠ ĐỒ TỔ CHỨC</a></li>
		              	<li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/phuong-cham-van-hanh/">PHƯƠNG CHÂM VẬN HÀNH</a></li>
		            	</ul>
		          	</li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/co-dong/">CỔ ĐÔNG</a>
			            <ul class='ushover'>
			              <li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/dieu-le-qui-che/">ĐIỀU LỆ - QUI CHẾ</a></li>
			              <li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/category/tin-tuc-co-dong/">TIN TỨC CỔ ĐÔNG</a></li>
			              <li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/bao-cao/">BÁO CÁO</a></li>
			            </ul>
		          	</li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/category/san-pham/">SẢN PHẨM</a></li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/category/dich-vu/">DỊCH VỤ</a></li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/category/doi-tac/">ĐỐI TÁC</a></li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/lien-he/">LIÊN HỆ</a></li>       
		        </ul>
		        <div class='navsearch-white' style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/searchwhite.png) no-repeat center center;">
		        </div>
		        <div class="rx-header-navsearch">
		          <input type="text" placeholder="Search">
		          <div class="iconsearch" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/search.png) no-repeat center center;"></div>
		        </div>
		        <div class='header_botclick'>
		          <div class="menu1_img" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/iconmenu.png) no-repeat center right;"></div>
		          <div class='botclick_hover'>
		            <div class='botclick_content1'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/ve-chung-toi/">VỀ CHÚNG TÔI</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/ve-chung-toi/">TỔNG QUAN</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/nhung-chan-duong-netacom/">NHỮNG CHẶN ĐƯỜNG NETACOM</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/so-do-to-chuc/">SƠ ĐỒ TỔ CHỨC</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/phuong-cham-van-hanh/">PHƯƠNG CHÂM VẬN HÀNH</a>
		            </div>
		            <div class='us_menu'></div>
		            <div class='botclick_content2'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/co-dong/">CỔ ĐÔNG</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/dieu-le-qui-che/">ĐIỀU LỆ - QUI CHẾ</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/category/tin-tuc-co-dong/">TIN TỨC CỔ ĐÔNG</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/bao-cao/">BÁO CÁO</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/san-pham/">SẢN PHẨM</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/dich-vu/">DỊCH VỤ</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/doi-tac/">ĐỐI TÁC</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/lien-he/">LIÊN HỆ</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/tin-tuc/">TIN TỨC</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/tuyen-dung/">TUYỂN DỤNG</a>
		            </div>
		          </div>
		    	</div>
		    </div>
    	</div>
	    
