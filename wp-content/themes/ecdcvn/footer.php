<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
		<div class="rx-block-newsletter">
		      <div class="rx-newsletter-container">
		        <div class="rx-newsletter-name">
		          <div class="rx-newsletter-name-top">ĐĂNG KÝ NHẬN TIN TỪ CHÚNG TÔI</div>
		        </div>
		        <input type="text" class="rx-newsletter-input" placeholder="Nhập thư điện tử">      
		        <button type="button" class="rx-newsletter-button">ĐĂNG KÝ</button>
		        <div class="rx-newsletter-social">
		          <a href="<?=get_site_url()?>" class="rx-icon-social rx-icon-facebook"></a>
		          <!-- <div class="rx-icon-social rx-icon-google"></div>
		          <div class="rx-icon-social rx-icon-twitter"></div> -->
		          <a href="<?=get_site_url()?>" class="rx-icon-social rx-icon-youtube"></a>
		          <!-- <div class="rx-icon-social rx-icon-instagram"></div>
		          <div class="rx-icon-social rx-icon-skype"></div> -->
		        </div>
		      </div>
		</div>

	    <div class="rx-footer">
	      <div class="rx-footer-container">
	        <div class="rx-footer-top">
	          <div class="rx-address-block">
	            <div class="rx-footer-block-name">Hồ Chí Minh</div>
	            <div class="rx-footer-address-container">
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-location"></div> 
	              	<div class='rx-footer-content'>Tầng Trệt, Toà nhà Centre Point <br/>106 Nguyễn Văn Trổi, Phường 8, Quận Phú Nhuận, TPHCM</div>
	              </div>
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-phone"></div> 
	              	<div class='rx-footer-content'>028 3535 4016</div>
	              </div>
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-email"></div>
	              	<div class='rx-footer-content'>contact@netacom.vn</div>
	              	</div>
	            </div>
	          </div>
	          <div class="rx-address-block">
	            <div class="rx-footer-block-name">Hà Nội</div>
	            <div class="rx-footer-address-container">
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-location"></div>
	              	<div class='rx-footer-content'>Phòng 712 tầng 7, Toà nhà văn phòng 362 Phố Huế <br/> Phường Phố Huế, Quận Hai Bà Trưng, Hà Nội, Vietnam</div>
	              </div>
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-email"></div>
	              	<div class='rx-footer-content'>hanoi@netacom.vn</div>
	              </div>              
	            </div>
	          </div>
	          <div class="rx-quicklink">
	            <div class="rx-footer-block-name">LIÊN KẾT</div>
	            <div class="rx-footer-quicklink-container">
	              <div class="rx-footer-quicklink-left">
	                <div>Trang chủ</div>
	                <div>Về chúng tôi</div>
	                <div>Đối tác</div>
	                <div>Dịch vụ</div>
	              </div>
	              <div class="rx-footer-quicklink-right">
	                <div>Sản phẩm</div>
	                <div>Tuyển dụng</div>
	                <div>Tin tức</div>
	                <div>Liên kết</div>
	              </div>
	            </div>
	          </div>
	        </div>
	        <div class="rx-footer-bot">
	          <div class="rx-col-lg-4"><div class="rx-footer-ecdc-privacy">Copyright © 2020 Netacom. All right reserved.</div></div>
	          <div class="rx-col-lg-4"><div class="rx-footer-ecdc-copyright"></div></div>
	        </div>
	      </div>
	    </div>
<?php wp_footer(); ?>
</body>
</html>
