<?php
/*
 *  Template Name: page Contact
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
  <?php endif; ?>
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="rx-page">
		<div class="rx-header-group">
			<div class="rx-header">
		     <div class="rx-ecdc-logo">
		        <a href="<?=get_site_url()?>" >
		          <div class="rx-ecdc-mainlogo" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/netacom2.jpg) no-repeat center center;"></div>
		        </a>
		      </div>
		      <div class="rx-header-top">
		        <div class="rx-header-logo">          
		        </div>
		        <div class="rx-header-navleft">
		          <div><span class="rx-icon-topnav rx-icon-phone"></span>028 3535 4016</div>
		          <div><span class="rx-icon-topnav rx-icon-email"></span>contact@netacom.vn</div>
		        </div>
		        <div class="rx-header-navright">
		          <div>
		            <a class='linktext' href="<?=get_site_url()?>">Trang chủ</a>
		          </div>
		          <div class='news'>
		            <a class='linktext' href="<?=get_site_url()?>/category/tin-tuc/">Tin tức</a>
		          </div>
		          <div class='tuyen'>
		            <a class='linktext' href="<?=get_site_url()?>/category/tuyen-dung/">Tuyển dụng</a>
		          </div>
		          <div class='lang'>
		            <a href="<?=get_site_url()?>" >
		              <div class="langimg" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/rx-ecdc-langviet.png) no-repeat center center;"></div>
		            </a>
		            <div class='langhover'>
		              <div class='langimghover'>
		                <a href="<?=get_site_url()?>" >
		                  <div class="langimghv" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/rx-ecdc-langviet.png) no-repeat center center;"></div>
		                </a>
		              </div>
		              <div class='langimghover'>
		                <a href="<?=get_site_url()?>" >
		                  <div class="langimghv" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/english.png) no-repeat center center;"></div>
		                </a>
		              </div>
		            </div>
		          </div>
		        </div>
		      </div>
		       
		    <div class="rx-header-bot">
		        <ul class="rx-header-navmain">
		          	<li class='header-menu-content'>
		          		<a class='linktexthd' href="<?=get_site_url()?>/ve-chung-toi/">VỀ CHÚNG TÔI</a>
						<ul class='ushover'>
		              	<li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/ve-chung-toi/">TỔNG QUAN</a></li>
		              	<li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/nhung-chan-duong-netacom/">NHỮNG CHẶN ĐƯỜNG NETACOM</a></li>
		           		<li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/so-do-to-chuc/">SƠ ĐỒ TỔ CHỨC</a></li>
		              	<li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/phuong-cham-van-hanh/">PHƯƠNG CHÂM VẬN HÀNH</a></li>
		            	</ul>
		          	</li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/co-dong/">CỔ ĐÔNG</a>
			            <ul class='ushover'>
			              <li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/dieu-le-qui-che/">ĐIỀU LỆ - QUI CHẾ</a></li>
			              <li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/category/tin-tuc-co-dong/">TIN TỨC CỔ ĐÔNG</a></li>
			              <li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/bao-cao/">BÁO CÁO</a></li>
			            </ul>
		          	</li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/category/san-pham/">SẢN PHẨM</a></li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/category/dich-vu/">DỊCH VỤ</a></li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/category/doi-tac/">ĐỐI TÁC</a></li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/lien-he/">LIÊN HỆ</a></li>       
		        </ul>
		        <div class='navsearch-white' style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/searchwhite.png) no-repeat center center;">
		        </div>
		        <div class="rx-header-navsearch">
		          <input type="text" placeholder="Search">
		          <div class="iconsearch" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/search.png) no-repeat center center;"></div>
		        </div>
		        <div class='header_botclick'>
		          <div class="menu1_img" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/iconmenu.png) no-repeat center right;"></div>
		          <div class='botclick_hover'>
		            <div class='botclick_content1'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/ve-chung-toi/">VỀ CHÚNG TÔI</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/ve-chung-toi/">TỔNG QUAN</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/nhung-chan-duong-netacom/">NHỮNG CHẶN ĐƯỜNG NETACOM</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/so-do-to-chuc/">SƠ ĐỒ TỔ CHỨC</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/phuong-cham-van-hanh/">PHƯƠNG CHÂM VẬN HÀNH</a>
		            </div>
		            <div class='us_menu'></div>
		            <div class='botclick_content2'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/co-dong/">CỔ ĐÔNG</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/dieu-le-qui-che/">ĐIỀU LỆ - QUI CHẾ</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/category/tin-tuc-co-dong/">TIN TỨC CỔ ĐÔNG</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/bao-cao/">BÁO CÁO</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/san-pham/">SẢN PHẨM</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/dich-vu/">DỊCH VỤ</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/doi-tac/">ĐỐI TÁC</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/lien-he/">LIÊN HỆ</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/tin-tuc/">TIN TỨC</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/tuyen-dung/">TUYỂN DỤNG</a>
		            </div>
		          </div>
		    	</div>
		    </div>
    	</div>
    		
    	<div class="rx-body-group">
    		<div class="rx-block-slider1">
		      <div class="rx-slider-items">
		        <div class="rx-slider-item" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/home_about.jpg) no-repeat center center;"></div>
		        <div class='content_title'>
		          <main id="main" class="site-main" role="main">
		          <?php
		            while ( have_posts() ) : the_post();
		          ?>
		          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		            <header class="entry-header">
		              <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		            </header>
		          </article>

		          <?php
		            if ( comments_open() || get_comments_number() ) {
		              comments_template();
		            }
		            the_post_navigation( array(
		              'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'twentysixteen' ),
		            ) );
		          endwhile;
		          ?>
		          </main>
		        </div>
		        
		      </div>
    		</div>
		    <div class='rx-body-contact'>
		    	<div class="contact-container">
		    		<div class="contact-groups">
			    		<div class="contact-img">
			    			<div class="image-mail" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/imgemail.png) no-repeat center center;"></div>
			    		</div>
			    		<div class="contact-content">
			    			<div class="contact-title">HỒ CHÍ MINH</div>
			    			<div class="contact-text">Tầng Trệt, Toà nhà Centre Point, 106 Nguyễn Văn Trổi, Phường 8, Quận Phú Nhuận, TPHCM</div>
			    			<div class="contact-text1">028 3535 4016</div>
			    			<div class="contact-text1">contact@netacom.vn</div>
			    		</div>
			    	</div>
			    	<div class="contact-groups">
			    		<div class="contact-img">
			    			<div class="image-mail" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/imgemail.png) no-repeat center center;"></div>
			    		</div>
			    		<div class="contact-content">
			    			<div class="contact-title">HÀ NỘI</div>
			    			<div class="contact-text">Phòng 712 tầng 7, Toà nhà văn phòng 362 Phố Huế,Phường Phố Huế, Quận Hai Bà Trưng, Hà Nội, Vietnam</div>
			    			<div class="contact-text1">hanoi@netacom.vn</div>
			    		</div>
			    	</div>
		    	</div>
		    </div>
		    
		    <div class="rx-block-newsletter">
		      <div class="rx-newsletter-container">
		        <div class="rx-newsletter-name">
		          <div class="rx-newsletter-name-top">ĐĂNG KÝ NHẬN TIN TỪ CHÚNG TÔI</div>
		        </div>
		        <input type="text" class="rx-newsletter-input" placeholder="Nhập thư điện tử">      
		        <button type="button" class="rx-newsletter-button">ĐĂNG KÝ</button>
		        <div class="rx-newsletter-social">
		          <a href="<?=get_site_url()?>" class="rx-icon-social rx-icon-facebook"></a>
		          <!-- <div class="rx-icon-social rx-icon-google"></div>
		          <div class="rx-icon-social rx-icon-twitter"></div> -->
		          <a href="<?=get_site_url()?>" class="rx-icon-social rx-icon-youtube"></a>
		          <!-- <div class="rx-icon-social rx-icon-instagram"></div>
		          <div class="rx-icon-social rx-icon-skype"></div> -->
		        </div>
		      </div>
		    </div>
		</div>
		<div class="rx-footer">
	      <div class="rx-footer-container">
	        <div class="rx-footer-top">
	          <div class="rx-address-block">
	            <div class="rx-footer-block-name">Hồ Chí Minh</div>
	            <div class="rx-footer-address-container">
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-location"></div> 
	              	<div class='rx-footer-content'>Tầng Trệt, Toà nhà Centre Point <br/>106 Nguyễn Văn Trổi, Phường 8, Quận Phú Nhuận, TPHCM</div>
	              </div>
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-phone"></div> 
	              	<div class='rx-footer-content'>028 3535 4016</div>
	              </div>
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-email"></div>
	              	<div class='rx-footer-content'>contact@netacom.vn</div>
	              	</div>
	            </div>
	          </div>
	          <div class="rx-address-block">
	            <div class="rx-footer-block-name">Hà Nội</div>
	            <div class="rx-footer-address-container">
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-location"></div>
	              	<div class='rx-footer-content'>Phòng 712 tầng 7, Toà nhà văn phòng 362 Phố Huế <br/> Phường Phố Huế, Quận Hai Bà Trưng, Hà Nội, Vietnam</div>
	              </div>
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-email"></div>
	              	<div class='rx-footer-content'>hanoi@netacom.vn</div>
	              </div>              
	            </div>
	          </div>
	          <div class="rx-quicklink">
	            <div class="rx-footer-block-name">LIÊN KẾT</div>
	            <div class="rx-footer-quicklink-container">
	              <div class="rx-footer-quicklink-left">
	                <div>Trang chủ</div>
	                <div>Về chúng tôi</div>
	                <div>Đối tác</div>
	                <div>Dịch vụ</div>
	              </div>
	              <div class="rx-footer-quicklink-right">
	                <div>Sản phẩm</div>
	                <div>Tuyển dụng</div>
	                <div>Tin tức</div>
	                <div>Liên kết</div>
	              </div>
	            </div>
	          </div>
	        </div>
	        <div class="rx-footer-bot">
	          <div class="rx-col-lg-4"><div class="rx-footer-ecdc-privacy">Copyright © 2020 Netacom. All right reserved.</div></div>
	          <div class="rx-col-lg-4"><div class="rx-footer-ecdc-copyright"></div></div>
	        </div>
	      </div>
	    </div>
	</div>

</body>
<?php wp_footer(); ?>