<?php
/*
 *  Template Name: Netacom Homepage
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
  <?php endif; ?>
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="rx-page">
		<div class="rx-header-group">
			<div class="rx-header">
		     <div class="rx-ecdc-logo">
		        <a href="<?=get_site_url()?>" >
		          <div class="rx-ecdc-mainlogo" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/netacom2.jpg) no-repeat center center;"></div>
		        </a>
		      </div>
		      <div class="rx-header-top">
		        <div class="rx-header-logo">          
		        </div>
		        <div class="rx-header-navleft">
		          <div><span class="rx-icon-topnav rx-icon-phone"></span>028 3535 4016</div>
		          <div><span class="rx-icon-topnav rx-icon-email"></span>contact@netacom.vn</div>
		        </div>
		        <div class="rx-header-navright">
		          <div>
		            <a class='linktext' href="<?=get_site_url()?>">Trang chủ</a>
		          </div>
		          <div class='news'>
		            <a class='linktext' href="<?=get_site_url()?>/category/tin-tuc/">Tin tức</a>
		          </div>
		          <div class='tuyen'>
		            <a class='linktext' href="<?=get_site_url()?>/category/tuyen-dung/">Tuyển dụng</a>
		          </div>
		          <div class='lang'>
		            <a href="<?=get_site_url()?>" >
		              <div class="langimg" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/rx-ecdc-langviet.png) no-repeat center center;"></div>
		            </a>
		            <div class='langhover'>
		              <div class='langimghover'>
		                <a href="<?=get_site_url()?>" >
		                  <div class="langimghv" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/rx-ecdc-langviet.png) no-repeat center center;"></div>
		                </a>
		              </div>
		              <div class='langimghover'>
		                <a href="<?=get_site_url()?>" >
		                  <div class="langimghv" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/english.png) no-repeat center center;"></div>
		                </a>
		              </div>
		            </div>
		          </div>
		        </div>
		      </div>
		       
		    <div class="rx-header-bot">
		        <ul class="rx-header-navmain">
		          	<li class='header-menu-content'>
		          		<a class='linktexthd' href="<?=get_site_url()?>/ve-chung-toi/">VỀ CHÚNG TÔI</a>
						<ul class='ushover'>
		              	<li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/ve-chung-toi/">TỔNG QUAN</a></li>
		              	<li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/nhung-chan-duong-netacom/">NHỮNG CHẶN ĐƯỜNG NETACOM</a></li>
		           		<li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/so-do-to-chuc/">SƠ ĐỒ TỔ CHỨC</a></li>
		              	<li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/phuong-cham-van-hanh/">PHƯƠNG CHÂM VẬN HÀNH</a></li>
		            	</ul>
		          	</li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/co-dong/">CỔ ĐÔNG</a>
			            <ul class='ushover'>
			              <li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/dieu-le-qui-che/">ĐIỀU LỆ - QUI CHẾ</a></li>
			              <li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/category/tin-tuc-co-dong/">TIN TỨC CỔ ĐÔNG</a></li>
			              <li class='uschild'><a class='linktext1' href="<?=get_site_url()?>/bao-cao/">BÁO CÁO</a></li>
			            </ul>
		          	</li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/category/san-pham/">SẢN PHẨM</a></li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/category/dich-vu/">DỊCH VỤ</a></li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/category/doi-tac/">ĐỐI TÁC</a></li>
		          	<li class='header-menu-content'><a class='linktexthd' href="<?=get_site_url()?>/lien-he/">LIÊN HỆ</a></li>       
		        </ul>
		        <div class='navsearch-white' style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/searchwhite.png) no-repeat center center;">
		        </div>
		        <div class="rx-header-navsearch">
		          <input type="text" placeholder="Search">
		          <div class="iconsearch" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/search.png) no-repeat center center;"></div>
		        </div>
		        <div class='header_botclick'>
		          <div class="menu1_img" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/iconmenu.png) no-repeat center right;"></div>
		          <div class='botclick_hover'>
		            <div class='botclick_content1'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/ve-chung-toi/">VỀ CHÚNG TÔI</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/ve-chung-toi/">TỔNG QUAN</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/nhung-chan-duong-netacom/">NHỮNG CHẶN ĐƯỜNG NETACOM</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/so-do-to-chuc/">SƠ ĐỒ TỔ CHỨC</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/phuong-cham-van-hanh/">PHƯƠNG CHÂM VẬN HÀNH</a>
		            </div>
		            <div class='us_menu'></div>
		            <div class='botclick_content2'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/co-dong/">CỔ ĐÔNG</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/dieu-le-qui-che/">ĐIỀU LỆ - QUI CHẾ</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/category/tin-tuc-co-dong/">TIN TỨC CỔ ĐÔNG</a>
		            </div>
		            <div class='botclick_content1'>
		              <a class='linktext_hover1' href="<?=get_site_url()?>/bao-cao/">BÁO CÁO</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/san-pham/">SẢN PHẨM</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/dich-vu/">DỊCH VỤ</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/doi-tac/">ĐỐI TÁC</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/lien-he/">LIÊN HỆ</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/tin-tuc/">TIN TỨC</a>
		            </div>
		            <div class='botclick_content'>
		              <a class='linktext_hover' href="<?=get_site_url()?>/category/tuyen-dung/">TUYỂN DỤNG</a>
		            </div>
		          </div>
		    	</div>
		    </div>
    	</div>
    		
    	<div class="rx-body-group">
			<div class="rx-block-slider">
		      <div class="rx-slider-items">
		        <div class="rx-slider-item" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/mainslider.jpg) no-repeat center center;"></div>
		        <div class="rx-slider-item1" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/Bitmap2.png) no-repeat center center;"></div>
		      </div>
		      <div onclick='left()' class="rx-slider-left">
		        <div class="rx-slider-leftimg" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/left1.png) no-repeat center center;"></div>
		      </div>
		      <div onclick='right()' class="rx-slider-right">
		        <div class="rx-slider-rightimg" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/right.png) no-repeat center center;"></div>
		      </div>
		    </div>
		    <div class="rx-block-service-group">
		    	 <div class="rx-block-service">
				      <div class="rx-block-service-heading"><span class="rx-black">NETACOM</span><span class="rx-red">MANG ĐẾN</span></div>
				      <div class="rx-block-service-desc">Trong kỷ nguyên số, NETACOM khẳng định rằng sở hữu trí tuệ và sự sáng tạo là tài sản quý giá nhất của công ty.</div>
				      <div class="rx-block-service-content">
				        <div class="rx-service-circle-wrap">
				          <div class="rx-service-circle">
				            <div class="rx-service-ico" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/iconphone.png) no-repeat center right;"></div>
				            <div class="rx-service-heading">NỀN TẢNG <br/> DI ĐỘNG</div>
				          </div>
				        </div>
				        <div class="rx-service-circle-wrap">
				          <div class="rx-service-circle">
				            <div class="rx-service-ico" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/iconfax.png) no-repeat center right;"></div>
				            <div class="rx-service-heading">NỘI DUNG <br/> DI ĐỘNG</div>
				          </div>
				        </div>
				        <div class="rx-service-circle-wrap">
				          <div class="rx-service-circle">
				            <div class="rx-service-ico" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/iconpay.png) no-repeat center right;"></div>
				            <div class="rx-service-heading">GIẢI PHÁP <br/> THANH TOÁN</div>
				          </div>
				        </div>
				        <div class="rx-service-circle-wrap">
				          <div class="rx-service-circle">
				            <div class="rx-service-ico" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/iconcskh.png) no-repeat center right;"></div>
				            <div class="rx-service-heading">CSKH 24/7</div>
				          </div>
				        </div>
				      </div>
				    </div>
		    </div>
		    <div class="rx-block-about">
		      <div class="rx-block-about-bg" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/rx-block-about.jpg) no-repeat center center;"></div>
		      <div class="rx-block-about-container">
		      	<div class="rx-about-nametop">NETACOM</div>
			      <div class="rx-about-name">KHỞI NGUỒN SÁNG TẠO</div>
			      <div class="rx-about-content">Thị trường dịch vụ truyền thông và viễn thông Việt Nam đang là thị trường khoa học công nghệ đầy tiềm năng phát triển. Sự phát triển như vũ bão của công nghệ thông tin đòi hỏi ngành viễn thông và truyền thông phải liên tục thay đổi, tạo khác biệt.<br/><br/>
			      NETACOM sẽ trở thành một tập đoàn hàng đầu Việt Nam và là một trong mười công ty đứng đầu khu vực Đông Nam Á về lĩnh vực nội dung số.
			      </div>
			      <div class="rx-about-readmore-container">
			        <div class="rx-about-readmore">Xem thêm</div>
			      </div>
		      </div>
		    </div>
		    <div class="rx-block-newsletter">
		      <div class="rx-newsletter-container">
		        <div class="rx-newsletter-name">
		          <div class="rx-newsletter-name-top">ĐĂNG KÝ NHẬN TIN TỪ CHÚNG TÔI</div>
		        </div>
		        <input type="text" class="rx-newsletter-input" placeholder="Nhập thư điện tử">      
		        <button class="rx-newsletter-button">ĐĂNG KÝ</button>
		        <div class="rx-newsletter-social">
		          <a href="<?=get_site_url()?>" class="rx-icon-social rx-icon-facebook"></a>
		          <!-- <div class="rx-icon-social rx-icon-google"></div>
		          <div class="rx-icon-social rx-icon-twitter"></div> -->
		          <a href="<?=get_site_url()?>" class="rx-icon-social rx-icon-youtube"></a>
		          <!-- <div class="rx-icon-social rx-icon-instagram"></div>
		          <div class="rx-icon-social rx-icon-skype"></div> -->
		        </div>
		      </div>
		    </div>
		</div>
		<div class="rx-footer">
	      <div class="rx-footer-container">
	        <div class="rx-footer-top">
	          <div class="rx-address-block">
	            <div class="rx-footer-block-name">Hồ Chí Minh</div>
	            <div class="rx-footer-address-container">
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-location"></div> 
	              	<div class='rx-footer-content'>Tầng Trệt, Toà nhà Centre Point <br/>106 Nguyễn Văn Trổi, Phường 8, Quận Phú Nhuận, TPHCM</div>
	              </div>
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-phone"></div> 
	              	<div class='rx-footer-content'>028 3535 4016</div>
	              </div>
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-email"></div>
	              	<div class='rx-footer-content'>contact@netacom.vn</div>
	              	</div>
	            </div>
	          </div>
	          <div class="rx-address-block">
	            <div class="rx-footer-block-name">Hà Nội</div>
	            <div class="rx-footer-address-container">
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-location"></div>
	              	<div class='rx-footer-content'>Phòng 712 tầng 7, Toà nhà văn phòng 362 Phố Huế <br/> Phường Phố Huế, Quận Hai Bà Trưng, Hà Nội, Vietnam</div>
	              </div>
	              <div class='rx-footer-container-group'>
	              	<div class="rx-icon-contact rx-icon-email"></div>
	              	<div class='rx-footer-content'>hanoi@netacom.vn</div>
	              </div>              
	            </div>
	          </div>
	          <div class="rx-quicklink">
	            <div class="rx-footer-block-name">LIÊN KẾT</div>
	            <div class="rx-footer-quicklink-container">
	              <div class="rx-footer-quicklink-left">
	                <div>Trang chủ</div>
	                <div>Về chúng tôi</div>
	                <div>Đối tác</div>
	                <div>Dịch vụ</div>
	              </div>
	              <div class="rx-footer-quicklink-right">
	                <div>Sản phẩm</div>
	                <div>Tuyển dụng</div>
	                <div>Tin tức</div>
	                <div>Liên kết</div>
	              </div>
	            </div>
	          </div>
	        </div>
	        <div class="rx-footer-bot">
	          <div class="rx-col-lg-4"><div class="rx-footer-ecdc-privacy">Copyright © 2020 Netacom. All right reserved.</div></div>
	          <div class="rx-col-lg-4"><div class="rx-footer-ecdc-copyright"></div></div>
	        </div>
	      </div>
	    </div>
	</div>


<script type="text/javascript">
  function left (){
    document.getElementsByClassName("rx-slider-item")[0].style.display = 'block';
    document.getElementsByClassName('rx-slider-item1')[0].style.display = 'none'
  }
  function right (){
    document.getElementsByClassName('rx-slider-item1')[0].style.display = 'block';
    document.getElementsByClassName('rx-slider-item')[0].style.display = 'none'
  }
  setInterval(function(){right()}, 3000);
  setInterval(function(){left()}, 6000);
  
</script>
</body>
<?php wp_footer(); ?>