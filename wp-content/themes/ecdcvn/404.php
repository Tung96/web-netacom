<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
	<div class="rx-block-slider1">
      <div class="rx-slider-items">
        <div class="rx-slider-item" style="background: url(<?=get_site_url()?>/wp-content/uploads/2017/10/home_about.jpg) no-repeat center center;"></div>
        <div class='content_title'>
          <main id="main" class="site-main" role="main">
          <?php
            while ( have_posts() ) : the_post();
          ?>
          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
              <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            </header>
          </article>

          <?php
            if ( comments_open() || get_comments_number() ) {
              comments_template();
            }
            the_post_navigation( array(
              'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'twentysixteen' ),
            ) );
          endwhile;
          ?>
          </main>
        </div>
        
      </div>
  </div>
  <div id="primary" class="content-area-nothing">
        <main id="main" class="site-main" role="main">

          <section class="error-404 not-found">
            <div class="not-found-text">Không tìm thấy dữ liệu</div>
          </section><!-- .error-404 -->

        </main><!-- .site-main -->

  </div><!-- .content-area -->
	

<?php get_sidebar(); ?>
<?php get_footer(); ?>
